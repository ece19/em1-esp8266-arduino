#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
 
#define PACKET_SIZE 8
#define TIMEOUT 10e3

const char *ssid = "Obi-lan Kenobi";
const char *pass = "HelloThere"; 
 
unsigned int local_port = 1504; // local port to listen for UDP packets
unsigned int client_port = 1505;
 
IPAddress ServerIP(192,168,4,1);
IPAddress ClientIP(192,168,4,2);
 
// A UDP instance to let us send and receive packets over UDP
WiFiUDP udp;
 
char packet_buffer[PACKET_SIZE];   //Where we get the UDP data
//=======================================================================
//                Setup
//=======================================================================
void setup()
{
    Serial.begin(230400); // Enter baudrate
    Serial.println();
    WiFi.softAP(ssid, pass);    //Create Access point
 
    //Start UDP
    //Serial.println("Starting UDP");
    udp.begin(local_port);
    //Serial.print("Local port: ");
    //Serial.println(udp.localPort());
}
//======================================================================
//                MAIN LOOP
//======================================================================
void loop()
{
    int cb = udp.parsePacket();
   
    if (!cb) {
      char send_buff[512]; //maximum buffer size

      //If serial data is recived send it to UDP
      if(Serial.available()>0) {
        // get data from UART until no bytes are available
        unsigned char i_send;
        unsigned char payload = Serial.read();
        unsigned long timer_start = millis();

        for(i_send = 1; i_send <= payload; i_send++) {
          while(!Serial.available()) {
            if ((millis() - timer_start) > TIMEOUT) {
               return;
            }
          }
          
          send_buff[i_send] = Serial.read();
        }

        send_buff[0] = payload;

        // Create UDP paket
        udp.beginPacket(ClientIP, client_port);
        udp.write(send_buff, i_send); //Send to PC 
        udp.endPacket();
        }
    }
    else {
      unsigned char i_rec, rec_len;
      rec_len = udp.read(packet_buffer, PACKET_SIZE); // read the packet into the buffer, we are reading only eight bytes 

      // write to UART
      for(i_rec = 0; i_rec < rec_len; i_rec++) {
        Serial.write(packet_buffer, PACKET_SIZE);
        i_rec++;
      }

    }

}
//======================================================================
